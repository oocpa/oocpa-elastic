package ca.on.health.oocpa.elastic.repository;

import java.util.List;


import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import ca.on.health.oocpa.elastic.model.ProviderModel;


public interface ProviderRepository extends ElasticsearchRepository<ProviderModel, Long> {
	    List<ProviderModel> findByfirstName(String text);
	    List<ProviderModel> findBylastName(String text);
	    List<ProviderModel> findBystakeholderNo(String text);
		

	

		

	}


