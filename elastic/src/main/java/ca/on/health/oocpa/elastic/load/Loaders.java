package ca.on.health.oocpa.elastic.load;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ca.on.health.oocpa.elastic.model.ProviderModel;
import ca.on.health.oocpa.elastic.model.Users;
import ca.on.health.oocpa.elastic.repository.ProviderRepository;
import ca.on.health.oocpa.elastic.repository.UsersRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class Loaders {
	 Logger logger = LoggerFactory.getLogger(Loaders.class);
    @Autowired
    ElasticsearchOperations operations;

    @Autowired
    ElasticsearchOperations operationsProvider;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    ProviderRepository providerRepository;

    @PostConstruct
    @Transactional
    public void loadAll(){

    	operationsProvider.putMapping(ProviderModel.class);
          
        logger.error("Loading Data");
       
        getData();
        
       // usersRepository.saveAll(getData());
    
        logger.error("Loading Completed");

    }

    public void getData() {
    	//Long id, String stakeHolderNo, String firstName,String lastName
    	providerRepository.save((new ProviderModel(1L, "1231231222", "John", "Doe")));
    	providerRepository.save((new ProviderModel(2L, "3453453455", "Hank", "Doe")));
    	providerRepository.save((new ProviderModel(3L, "2342342344", "Jane", "Doe")));
    	providerRepository.save((new ProviderModel(4L, "5654654566", "Steve", "Doe")));
       	providerRepository.save((new ProviderModel(5L, "5467456777", "John", "Smith")));
    	providerRepository.save((new ProviderModel(6L, "4544231764", "Hank", "Smith")));
    	providerRepository.save((new ProviderModel(7L, "4523452435", "Jane", "South")));
    	providerRepository.save((new ProviderModel(8L, "5781234565", "Steve", "Smitty")));
    	providerRepository.save((new ProviderModel(9L, "9999999999", "Steve", "Smith")));
      
 
        
    }
  
}
