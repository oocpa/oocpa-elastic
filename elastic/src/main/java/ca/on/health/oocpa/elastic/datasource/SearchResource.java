package ca.on.health.oocpa.elastic.datasource;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONPObject;

import ca.on.health.oocpa.elastic.config.ElasticConfiguration;
import ca.on.health.oocpa.elastic.load.Loaders;
import ca.on.health.oocpa.elastic.model.ProviderModel;
import ca.on.health.oocpa.elastic.model.Users;
import ca.on.health.oocpa.elastic.repository.ProviderRepository;
import ca.on.health.oocpa.elastic.repository.UsersRepository;
import ch.qos.logback.core.net.server.Client;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/search")
public class SearchResource {
    Logger logger = LoggerFactory.getLogger(ElasticConfiguration.class);
	@Autowired
	ElasticConfiguration client;

    @Autowired
    ProviderRepository usersRepository;

    @GetMapping(value = "/global/{text}")

    
    public List<String> searchName(@PathVariable final String text) throws Exception {
       	
    	
    	  List<String> providers = new ArrayList<>();
    	 // build elasticsearch search request
        SearchRequestBuilder builder = client.client().prepareSearch("providers");
        builder.setTypes("providers");
        builder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        builder.setQuery(QueryBuilders.simpleQueryStringQuery(text +"*"));
  
        
        SearchResponse res = builder.get();
        SearchHit[] results = res.getHits().getHits();
        for(SearchHit hit : results){

            String sourceAsString = hit.getSourceAsString();
            if (sourceAsString != null) {
             	logger.error("Search Result: " + sourceAsString);
            	providers.add(sourceAsString);
              
            }
        }
       // SearchHits hits = res.getHits();
     
       
        return providers;
    }


    @GetMapping(value = "/stakeholder/{stakeno}")
    public List<ProviderModel> searchSalary(@PathVariable final String stakeno) {
    	logger.error("stakeHolder: " + stakeno);//
        return usersRepository.findBystakeholderNo(stakeno);
    }

    @GetMapping(value = "/firstname/{name}")
    public List<ProviderModel> searchFirstName(@PathVariable final String name) {
    	logger.error("Name: " + name);
        return usersRepository.findByfirstName(name);
    }

    @GetMapping(value = "/all")
    public List<ProviderModel> searchAll() {
        List<ProviderModel> usersList = new ArrayList<>();
        Iterable<ProviderModel> userses = usersRepository.findAll();
      	
        userses.forEach(usersList::add);
       for(int ctr = 0; ctr < usersList.size(); ctr++)
       {
    	   logger.error("All: " + usersList.get(ctr).toString());
       }
        return usersList; 
    }


}