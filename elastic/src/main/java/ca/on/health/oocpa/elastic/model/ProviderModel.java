package ca.on.health.oocpa.elastic.model;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "providers", type = "providers", shards = 1)
public class ProviderModel {
    private Long id;
	private String stakeholderNo;
	private String firstName;
	private String lastName;
	  public ProviderModel(Long id, String stakeHolderNo, String firstName,String lastName) {

	        this.stakeholderNo = stakeHolderNo;
	        this.id = id;
	        this.firstName = firstName;
	        this.lastName = lastName;
	    }
	  
	  public ProviderModel() {

	       
	    }

	public Long getId() 
	   {
	        return id;
	    }

	public void setId(Long id) {
	        this.id = id;
	    }
	    
	public String getStakeholderNo() {
		return stakeholderNo;
	}
	public void setStakeholderNo(String stakeholderNo) {
		this.stakeholderNo = stakeholderNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String toString()
	{
		String stringValue = "{ID:" + this.getId() + ", Stakeholder:"  + this.getStakeholderNo() + ", FirstName:" + this.getFirstName() + ", LastName:" + this.getLastName() +"}"; 
		return stringValue;
		
	}
}
