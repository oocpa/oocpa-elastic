package ca.on.health.oocpa.elastic.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;



@Configuration
@EnableElasticsearchRepositories(basePackages = "ca.on.health.oocpa.elastic.repository")
@ComponentScan(basePackages = { "ca.on.health.oocpa.elastic" })
public class ElasticConfiguration {
	
	  Logger logger = LoggerFactory.getLogger(ElasticConfiguration.class);
    @Value("${elasticsearch.host}")
    private String Host;

    @Value("${elasticsearch.port}")
    private int Port;

    @Value("${elasticsearch.clustername}")
    private String esClusterName;

    
    @Bean
    public Client client() throws Exception {
    	
        Settings settings = Settings.builder().put("cluster.name", esClusterName).build();
        TransportClient client = new PreBuiltTransportClient(settings);
        client.addTransportAddress(new TransportAddress(InetAddress.getByName(Host), Port));
      
        logger.error("Host: " + Host);
        
        return client;
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() throws Exception {
        return new ElasticsearchTemplate(client());
    }
}